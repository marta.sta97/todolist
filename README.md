# ToDoList Mobile App

## Table of contents
* [General info](#general-info)
* [Project purpose](#project-purpose)
* [Installation](#installation)

## General info
With this app you can easily manage your "To Do" tasks

More details:  
* Clear interface with pleasing to the eye icons  
* Quick adding and deleting tasks  
* Colorful task priorities  
* Simple task editing  
* Notifications  
* Search bar

## Project purpose
Building an app using Android Architecture Components:   
Room, ViewModel, LiveData

Creating and managing app notifications

## Installation
1. Download this project as zip and extract it
2. Import it in Android Studio
3. Sync Gradle and run on your device/emulator
