package com.codingdragons.todolist.Notifications

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

class NotificationUtils {
    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: PendingIntent
    private lateinit var context: Context

    fun setNotificationAlarm(activity: Activity, date: String, taskId: Int, taskName: String) {
        cancelNotificationAlarm(activity, taskId, taskName)

        if (date.isNotBlank()) {
            val calendar: Calendar = Calendar.getInstance().apply {
                timeInMillis = System.currentTimeMillis()
                time = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).parse(date)
            }

            alarmMgr?.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                alarmIntent
            )

            Toast.makeText(context, "Powiadomienie ustawione", Toast.LENGTH_SHORT).show()
        }
    }

    fun cancelNotificationAlarm(activity: Activity, taskId: Int, taskName: String) {
        context = activity.applicationContext
        alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(context, AlarmReceiver::class.java).let { intent ->
            intent.putExtra("id", taskId.toString())
            intent.putExtra("name", taskName)
            PendingIntent.getBroadcast(context, taskId, intent, 0)
        }

        alarmMgr?.cancel(alarmIntent)
    }

}