package com.codingdragons.todolist.Notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val service = Intent(context, NotificationService::class.java)
        service.putExtra("id", intent.getStringExtra("id"))
        service.putExtra("name", intent.getStringExtra("name"))
        NotificationService().enqueueWork(context, service)
    }

}