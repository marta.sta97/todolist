package com.codingdragons.todolist.Notifications

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import android.app.NotificationManager
import android.app.NotificationChannel
import androidx.core.app.JobIntentService
import com.codingdragons.todolist.Utils.Constants.Companion.CHANNEL_DESCRIPTION
import com.codingdragons.todolist.Utils.Constants.Companion.CHANNEL_ID
import com.codingdragons.todolist.Utils.Constants.Companion.CHANNEL_NAME
import com.codingdragons.todolist.Utils.Constants.Companion.JOB_ID
import com.codingdragons.todolist.MainActivity
import com.codingdragons.todolist.R

class NotificationService : JobIntentService() {

    private lateinit var notification: Notification

    fun enqueueWork(context: Context, intent: Intent) {
        enqueueWork(context, NotificationService::class.java, JOB_ID, intent)
    }

    override fun onHandleWork(intent: Intent) {
        val id = intent.getStringExtra("id").toInt()
        val name = intent.getStringExtra("name")
        createNotificationChannel()
        createNotification(name)

        with(NotificationManagerCompat.from(this)) {
            notify(id, notification) //show notification
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT

            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance).apply {
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC //shows the notification's full content
                description = CHANNEL_DESCRIPTION
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createNotification(name: String) {
        val context = this.applicationContext
        val title = "Masz zadanie do wykonania!"

        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra("title", title)
            putExtra("notification", true)
        }.let { intent -> PendingIntent.getActivity(context, 0, intent, 0) }

        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.list)
            .setContentTitle(title)
            .setContentText(name)
            .setSound(uri)
            .setAutoCancel(true)// Automatically removes the notification when the user taps it
            .setContentIntent(notifyIntent) // Set the intent that will fire when the user taps the notification
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()
    }

}