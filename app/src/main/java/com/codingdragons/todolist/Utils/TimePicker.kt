package com.codingdragons.todolist.Utils

import android.app.TimePickerDialog
import android.content.Context
import android.widget.TextView
import java.util.*

class TimePicker(private val context: Context) {

    private val c = Calendar.getInstance()

    fun setTime(textView: TextView) {
        val time = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            textView.text = String.format("%02d:%02d", hourOfDay, minute)
        }

        TimePickerDialog(context, time, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
    }
}