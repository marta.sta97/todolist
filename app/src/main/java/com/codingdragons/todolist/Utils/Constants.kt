package com.codingdragons.todolist.Utils

import com.codingdragons.todolist.Room.Task

class Constants {
    companion object{
        // Log
        const val TAG = "cd2019"

        // Request codes
        const val EDIT_ACTIVITY_REQUEST_CODE = 777

        // Notification
        const val CHANNEL_ID = "com.codingdragons.todolist.Notifications.CHANNEL_ID"
        const val CHANNEL_NAME = "ToDoList Notification Channel"
        const val CHANNEL_DESCRIPTION = "ToDoList Notification Channel Description"
        const val JOB_ID = 888


        // Dummy
        val dummyTasks: Array<Task> = arrayOf( //todo add createdAt?
            Task("Tutorial 1", null, "Kliknij zadanie, aby je edytować", "idea", 0, null, null, false),
            Task("Tutorial 2", null, "Przytrzymaj zadanie, aby je usunąć", "idea", 0, null, null, false),
            Task("Dodaj nowe zadanie", null, null, "list", 2, null, null, false)
        )
    }
}