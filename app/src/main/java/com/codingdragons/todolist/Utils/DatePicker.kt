package com.codingdragons.todolist.Utils

import android.app.DatePickerDialog
import android.content.Context
import android.widget.TextView
import java.util.*

class DatePicker(private val context: Context) {

    private val c = Calendar.getInstance()

    fun setDate(textView: TextView) {
        val date = DatePickerDialog.OnDateSetListener { _, year, month, day ->
            textView.text = String.format("%02d/%02d/%02d", day, month + 1, year) //todo date format
        }

        DatePickerDialog(
            context, date,
            c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH)
        ).show()
    }
}