package com.codingdragons.todolist.RecyclerView

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codingdragons.todolist.Utils.Constants.Companion.EDIT_ACTIVITY_REQUEST_CODE
import com.codingdragons.todolist.MainActivity
import com.codingdragons.todolist.Notifications.NotificationUtils
import com.codingdragons.todolist.R
import com.codingdragons.todolist.Room.Task
import com.codingdragons.todolist.TaskEditActivity
import com.codingdragons.todolist.TaskViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.task_layout.*

class TaskListAdapter(
    private var data: List<Task>,
    private val context: Context,
    private val taskViewModel: TaskViewModel
) : RecyclerView.Adapter<TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.task_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val task = data[position]

        holder.bind(task, context)

        holder.constraintLayout.apply {
            setOnLongClickListener {
                taskViewModel.delete(task)
                NotificationUtils().cancelNotificationAlarm(context as MainActivity, task.id.toInt(), task.name)
                true
            }

            setOnClickListener {
                val intent = Intent(context, TaskEditActivity::class.java)
                intent.putExtra("task", Gson().toJson(task))
                (context as MainActivity).startActivityForResult(intent, EDIT_ACTIVITY_REQUEST_CODE)
            }
        }
    }

    fun setTasks(tasks: List<Task>) {
        data = tasks
        notifyDataSetChanged()
    }

}