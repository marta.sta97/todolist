package com.codingdragons.todolist.RecyclerView

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codingdragons.todolist.Room.Task
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.task_layout.*

class TaskViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(task: Task, context: Context) {
        taskName.text = task.name
        priorityView.setImageResource(
            context.resources.getIdentifier(
                "priority${task.priority}",
                "color",
                context.packageName
            )
        )
        taskIcon.contentDescription = task.img
        taskIcon.setImageResource(context.resources.getIdentifier(task.img, "drawable", context.packageName))

        setDescView(taskDesc, task.desc)
        setDateTimeView(taskDate, task.date, task.time)
    }

    private fun setDateTimeView(view: TextView, date: String?, time: String?) {
        when {
            !date.isNullOrBlank() -> {
                view.visibility = View.VISIBLE
                view.text = "$date $time"
            }
            !time.isNullOrBlank() -> {
                view.visibility = View.VISIBLE
                view.text = time
            }
            else -> view.visibility = View.GONE
        }
    }

    private fun setDescView(view: TextView, desc: String?) {
        view.text = desc
        view.visibility = when (view.text) {
            "" -> View.GONE
            else -> View.VISIBLE
        }
    }

}