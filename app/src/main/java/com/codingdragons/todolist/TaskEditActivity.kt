package com.codingdragons.todolist

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_task_edit.*
import android.text.method.ScrollingMovementMethod
import android.view.WindowManager
import com.codingdragons.todolist.Notifications.NotificationUtils
import com.codingdragons.todolist.Room.Task
import com.codingdragons.todolist.Utils.DatePicker
import com.codingdragons.todolist.Utils.TimePicker
import com.codingdragons.todolist.Utils.validate
import com.google.gson.Gson

class TaskEditActivity : AppCompatActivity() {

    private lateinit var task: Task

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_edit)

        //Hide the keyboard on activity entry
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        taskDesc.movementMethod = ScrollingMovementMethod()

        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.task_edit)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val taskJson = intent.getStringExtra("task")
        task = Gson().fromJson(taskJson, Task::class.java)
        loadTask(task)

        setUpSeekBar()
        setUpDeleteButtons()
        setUpPickerDialogs()
        setUpNotificationCheckBox()
    }

    private fun setUpSeekBar() {
        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                changeProgressColor(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun setUpDeleteButtons() {
        buttonDeleteDate.setOnClickListener {
            taskDate.text = null
            taskTime.text = null
        }

        buttonDeleteDesc.setOnClickListener {
            taskDesc.text = null
        }
    }

    private fun setUpPickerDialogs() {
        val dp = DatePicker(this)
        val tp = TimePicker(this)

        calendarButton.setOnClickListener {
            dp.setDate(taskDate)
        }

        clockButton.setOnClickListener {
            tp.setTime(taskTime)
        }
    }

    private fun setUpNotificationCheckBox() {
        toggleCheckBox()
        taskDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                toggleCheckBox()
            }
        })
    }

    private fun toggleCheckBox() {
        if (!taskDate.text.isNullOrBlank()) {
            checkBox.visibility = View.VISIBLE
        } else {
            checkBox.visibility = View.GONE
            checkBox.isChecked = false
        }
    }

    private fun loadTask(t: Task) {
        taskName.setText(t.name)
        taskDate.text = t.date
        taskTime.text = t.time
        taskDesc.setText(t.desc)
        checkBox.isChecked = task.notify
        taskIcon.setImageResource(resources.getIdentifier(t.img, "drawable", packageName))
        seekBar!!.progress = t.priority
        changeProgressColor(t.priority)
    }

    private fun changeProgressColor(p: Int) {
        task.priority = p
        val color =
            ContextCompat.getColor(applicationContext, resources.getIdentifier("priority$p", "color", packageName))
        seekBar!!.thumb.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        seekBar!!.progressDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN)
    }

    fun changeIcon(view: View) {
        taskIcon.setImageResource(resources.getIdentifier(view.contentDescription.toString(), "drawable", packageName))
        taskIcon.contentDescription = view.contentDescription.toString()
        task.img = taskIcon.contentDescription.toString()
    }

    private fun onSave() {
        taskName.validate(getString(R.string.name_cannot_be_empty)) { it.isNotBlank() }
        if (taskName.text.isNotBlank()) {
            task.name = taskName.text.toString()
            task.desc = taskDesc.text.toString()
            val date = taskDate.text.toString()
            val time = taskTime.text.toString()
            if (task.notify != checkBox.isChecked || task.date != date || task.time != time) { //todo
                task.notify = checkBox.isChecked
                setUpOrCancelNotification(task.notify, date, time)
            }
            task.date = date
            task.time = time
            sendResult()
        }
    }

    private fun setUpOrCancelNotification(notify: Boolean, date: String, time: String?) {
        if (notify) {
            val notificationDate = if (time.isNullOrBlank()) {
                "$date 08:00"
            } else {
                "$date $time"
            }
            NotificationUtils().setNotificationAlarm(
                this,
                notificationDate,
                task.id.toInt(),
                task.name
            )
        } else {
            NotificationUtils().cancelNotificationAlarm(this, task.id.toInt(), task.name)
        }
    }

    private fun sendResult() {
        val intent = Intent()
        intent.putExtra("task", Gson().toJson(task))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_save -> {
            onSave()
            true
        }
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}
