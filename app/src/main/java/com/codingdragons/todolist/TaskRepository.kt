package com.codingdragons.todolist

import com.codingdragons.todolist.Room.Task
import com.codingdragons.todolist.Room.TaskDao
import android.app.Application
import androidx.lifecycle.LiveData
import com.codingdragons.todolist.Room.TaskRoomDatabase
import android.os.AsyncTask.execute
import android.util.Log
import com.codingdragons.todolist.Utils.Constants.Companion.TAG
import java.lang.Exception

/*
    Repository handles data operations and is a mediator between different data sources.
    It provides a clean API so that the rest of the app can retrieve this data easily.
 */

class TaskRepository (application: Application){

    private var taskDao: TaskDao

    init{
        val db = TaskRoomDatabase.getDatabase(application)
        taskDao = db.taskDao()
    }

    companion object{
        private var INSTANCE: TaskRepository? = null
        fun getRepository(application: Application): TaskRepository {
            if (INSTANCE == null) {
                INSTANCE = TaskRepository(application)
            }
            return INSTANCE as TaskRepository
        }
    }

    fun insert(task: Task) {
        execute {
            try {
                taskDao.insert(task)
            } catch (e: Exception) {
                Log.i(TAG, e.message)
            }
        }
    }

    fun update(task: Task) {
        execute {
            try {
                taskDao.update(task)
            } catch (e: Exception) {
                Log.i(TAG, e.message)
            }
        }
    }

    fun delete(task: Task){
        execute {
            try {
                taskDao.delete(task)
            } catch (e: Exception) {
                Log.i(TAG, e.message)
            }
        }
    }

    fun findTasks(name: String):  LiveData<List<Task>>{
        return taskDao.findTasks("%$name%")
    }

}