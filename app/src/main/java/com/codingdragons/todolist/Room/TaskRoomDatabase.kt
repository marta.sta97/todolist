package com.codingdragons.todolist.Room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.codingdragons.todolist.Utils.Constants.Companion.dummyTasks
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@Database(entities = [(Task::class)], version = 1)
abstract class TaskRoomDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao

    companion object {
        private var INSTANCE: TaskRoomDatabase? = null
        fun getDatabase(context: Context): TaskRoomDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    TaskRoomDatabase::class.java,
                    "tasks"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(roomDatabaseCallback)
                    .build()
            }
            return INSTANCE as TaskRoomDatabase
        }

        private val roomDatabaseCallback = object : RoomDatabase.Callback() {

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                GlobalScope.launch {
                    val taskDao = INSTANCE!!.taskDao()
//                    taskDao.deleteAll() //Clear database
                    taskDao.insertAll(*dummyTasks) // * converts Array to varargs
                }
            }
        }
    }
}