package com.codingdragons.todolist.Room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class Task(
    @ColumnInfo(name = "name") var name : String,
    @ColumnInfo(name = "createdAt") var createdAt : String?,
    @ColumnInfo(name = "desc") var desc : String?,
    @ColumnInfo(name = "img")  var img : String,
    @ColumnInfo(name = "priority")  var priority : Int,
    @ColumnInfo(name = "date")  var date : String?,
    @ColumnInfo(name = "time")  var time : String?,
    @ColumnInfo(name = "notify")  var notify: Boolean,
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
)