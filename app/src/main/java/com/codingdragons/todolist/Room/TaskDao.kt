package com.codingdragons.todolist.Room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TaskDao {

    @Query("SELECT * FROM tasks")
    fun getAll() : LiveData<List<Task>>

    @Insert
    fun insert(task : Task)

    @Delete
    fun delete(task: Task)

    @Update
    fun update(task : Task)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg task : Task)

    @Update
    fun updateAll(vararg task : Task)

    @Query("DELETE FROM tasks")
    fun deleteAll()

    @Query(
        "SELECT * from tasks " +
                "WHERE name LIKE :name"
    )
    fun findTasks(name : String) : LiveData<List<Task>>

}