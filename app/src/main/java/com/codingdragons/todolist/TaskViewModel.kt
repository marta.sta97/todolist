package com.codingdragons.todolist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.codingdragons.todolist.Room.Task

/*
    The ViewModel's role is to provide data to the UI and survive configuration changes.
    A ViewModel acts as a communication center between the Repository and the UI.
    You can also use a ViewModel to share data between fragments.
 */

class TaskViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: TaskRepository = TaskRepository.getRepository(application)
    private val query = MutableLiveData<String>()

    fun insert(task: Task) {
        repository.insert(task)
    }

    fun delete(task: Task) {
        repository.delete(task)
    }

    fun update(task: Task) {
        repository.update(task)
    }

    fun findTasks(name: String) = apply {
        query.value = name
    }

    val findTasksResult: LiveData<List<Task>> = Transformations.switchMap(query) { name ->
        repository.findTasks(name)
    }

}