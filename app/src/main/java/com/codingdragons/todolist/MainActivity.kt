package com.codingdragons.todolist

import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.codingdragons.todolist.RecyclerView.TaskListAdapter
import com.codingdragons.todolist.Room.Task
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import com.codingdragons.todolist.Utils.Constants.Companion.EDIT_ACTIVITY_REQUEST_CODE

class MainActivity : AppCompatActivity() {

    //todo filtr po ikonkach
    //todo poprawić ikonki

    private var taskList: List<Task> = ArrayList()
    private lateinit var taskListAdapter: TaskListAdapter
    private lateinit var taskViewModel: TaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)

        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel::class.java)

        taskListAdapter = TaskListAdapter(taskList, this, taskViewModel)

        // The onChanged() method fires when the observed data changes and the activity is in the foreground.
        taskViewModel.findTasksResult.observe(this, androidx.lifecycle.Observer<List<Task>> { tasks: List<Task> ->
            taskListAdapter.setTasks(tasks)
        })

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = taskListAdapter
            addItemDecoration(
                DividerItemDecoration(
                    recyclerView.context, DividerItemDecoration.VERTICAL
                )
            )
        }

        buttonAdd.setOnClickListener {
            addNewTask()
        }

        taskViewModel.findTasks("") //get all tasks
    }

    private fun addNewTask() {
        val name = editTextView.text.toString()
        if (name.isNotBlank()) {
            val createdAt = SimpleDateFormat("dd/MM/yyyy HH:mm:sss", Locale.getDefault()).format(Date())
            val t = Task(
                name.capitalize(),
                createdAt,
                null,
                "list",
                0,
                null, null,
                false
            )
            taskViewModel.insert(t)
            editTextView.text = null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_CANCELED) {
            if (requestCode == EDIT_ACTIVITY_REQUEST_CODE) {
                if (data != null) {
                    val taskJson = data.getStringExtra("task")
                    val task = Gson().fromJson(taskJson, Task::class.java)
                    taskViewModel.update(task)
                    editTextView.text.clear()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val searchItem = menu.findItem(R.id.app_bar_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            val searchEditText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text).apply {
                hint = getString(R.string.search)
            }

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    taskViewModel.findTasks(searchEditText.text.toString())
                    return true
                }
            })
        }

        return super.onCreateOptionsMenu(menu)
    }


    /*
        Rotate
    */
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        //TODO
    }
}
